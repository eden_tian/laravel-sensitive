<?php

namespace TianYong2\LaravelSensitive;

use Illuminate\Support\ServiceProvider;
use TianYong2\LaravelSensitive\Sensitive;

class SensitiveServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            Sensitive::class,
            function ($app) {
                $sensitive = new Sensitive();
                $interference = ['&', '*'];
                $filename = \Illuminate\Support\Facades\Storage::disk("public")->path(
                    "sensitive_words.txt"
                ); //每个敏感词独占一行
                $sensitive->interference($interference); //添加干扰因子
                $sensitive->addwords($filename); //需要过滤的敏感词
                return $sensitive;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Sensitive::class, 'sensitive'];
    }
}
