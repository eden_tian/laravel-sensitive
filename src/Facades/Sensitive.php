<?php

namespace TianYong2\LaravelSensitive\Facades;

use Illuminate\Support\Facades\Facade;
use TianYong2\LaravelSensitive\Sensitive as Accessor;

class Sensitive extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Accessor::class;
    }
}